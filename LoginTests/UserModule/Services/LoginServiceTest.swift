//
//  LoginServiceTest.swift
//  Login
//
//  Created by Sharvan Kumawat on 15/08/22.
//

import XCTest

@testable import Login

class LoginServiceTest: XCTestCase {
    
    struct ErrorMessage {
        static let kFailedErrorMeesage = "Login Failed Error"
    }
    
    var loginService: LoginServiceImpl!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        loginService = LoginServiceImpl()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testService_Success() {
        let promise = expectation(description: "Login success case")
        loginService.makeNetworkRequest(email: Constant.email, password: Constant.password)
            .done { result in
                if result == true {
                    promise.fulfill()
                }
            }
            .catch { _ in
                promise.fulfill()
            }
        wait(for: [promise], timeout: 2.0)
    }
}
