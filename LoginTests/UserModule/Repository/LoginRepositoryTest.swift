//
//  LoginRepositoryTest.swift
//  Login
//
//  Created by Sharvan Kumawat on 15/08/22.
//

import XCTest

@testable import Login

class LoginRepositoryTest: XCTestCase {
    
    struct ErrorMessage {
        static let kFailedErrorMeesage = "Repository Failed Error"
    }
    
    var loginRepository: LoginRepositoryImpl!
    let mockService = MockServiceTest()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        loginRepository = LoginRepositoryImpl(service: mockService)
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testRepository_Success() {
        let promise = expectation(description: "Login Repository on success")
        
        loginRepository.makeServiceCallToLoginUser(email: Constant.email, password: Constant.password)
            .done { result in
                if result == true {
                    promise.fulfill()
                }
            }
            .catch { _ in
                promise.fulfill()
            }
        
        wait(for: [promise], timeout: 2.0)
    }
    
    func testRepository_Failure() {
        let promise = expectation(description: "Login Repository on Failed")
        
        mockService.error = NSError(domain: "com.example.error", code: 0, userInfo: [NSLocalizedDescriptionKey: ErrorMessage.kFailedErrorMeesage])
        
        loginRepository.makeServiceCallToLoginUser(email: Constant.email, password: Constant.password)
            .catch {error in
                XCTAssertTrue(error.localizedDescription == ErrorMessage.kFailedErrorMeesage)
                promise.fulfill()
            }
        
        wait(for: [promise], timeout: 2.0)
    }
    
}
