//
//  RegisterViewModelTest.swift
//  Login
//
//  Created by Sharvan Kumawat on 05/08/22.
//

import XCTest
@testable import Login

class RegisterViewModelTest: XCTestCase {
    
    var viewModel: RegisterViewModel?
    
    override func setUp() {
        super.setUp()
        viewModel = RegisterViewModel()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testViewModel_Success() {
        viewModel?.errorMessage = ""
        viewModel?.registerUser(email: Constant.email, password: Constant.password, completion: { result in
            XCTAssertTrue(result == true)
        })
    }
    
    func testViewModel_Fail() {
        viewModel?.errorMessage = "Email already register"
        viewModel?.registerUser(email: Constant.email, password: Constant.password, completion: { result in
            XCTAssertTrue(result == false)
        })
    }
    
    
}
