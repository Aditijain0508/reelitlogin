//
//  MockUseCase.swift
//  LoginTests
//
//  Created by Sharvan Kumawat on 05/08/22.
//

import Foundation
import PromiseKit

class MockUseCase: ILoginUseCase {
    var error: Error?
    
    func getLogin(email: String, password: String) -> UserResponse {
        return Promise { seal in
            if let error = error {
                seal.reject(error)
            } else {
                seal.fulfill(true)
            }
        }
    }
}
