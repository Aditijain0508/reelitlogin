//
//  MockServiceTest.swift
//  Login
//
//  Created by Sharvan Kumawat on 15/08/22.
//

import Foundation
import PromiseKit

class MockServiceTest: ILoginService {
    
    var error: Error?
    
    func makeNetworkRequest(email: String, password: String) -> UserResponse {
        return Promise { seal in
            if let error = error {
                seal.reject(error)
            } else {
                seal.fulfill(true)
            }
        }
    }
}
