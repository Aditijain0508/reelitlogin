//
//  MockRepository.swift
//  Login
//
//  Created by Sharvan Kumawat on 04/07/22.
//

import Foundation
import PromiseKit

class MockRepository: ILoginRepository {
    
    var error: Error?
    
    func makeServiceCallToLoginUser(email: String, password: String) -> UserResponse {
        return Promise { seal in
            if let error = error {
                seal.reject(error)
            } else {
                seal.fulfill(true)
            }
        }
    }
}
