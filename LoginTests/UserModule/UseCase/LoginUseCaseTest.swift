//
//  LoginUseCaseTest.swift
//  Login
//
//  Created by Sharvan Kumawat on 15/08/22.
//

import XCTest
@testable import Login


class LoginUseCaseTest: XCTestCase {
    
    struct ErrorMessage {
        static let kFailedErrorMeesage = "Use Case Failed Error"
    }
    
    var useCase: LoginUseCaseImpl!
    let repository = MockRepository()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        useCase = LoginUseCaseImpl(repository: repository)
    }
    
    func testUseCase_Success() {
        let promise = expectation(description: "Login success")
        
        useCase.getLogin(email: Constant.email, password: Constant.password)
            .done { model in
                if model == true {
                    promise.fulfill()
                }
            }
            .catch { _ in }
        
        wait(for: [promise], timeout: 2.0)
    }
    
    func testUseCase_Failure() {
        let promise = expectation(description: "Login failed")
        repository.error = NSError(domain: "com.example.error", code: 0, userInfo: [NSLocalizedDescriptionKey: ErrorMessage.kFailedErrorMeesage])
        
        useCase.getLogin(email: Constant.email, password: Constant.password)
            .catch { error in
                XCTAssertTrue(error.localizedDescription == ErrorMessage.kFailedErrorMeesage)
                promise.fulfill()
            }
        
        wait(for: [promise], timeout: 2.0)
    }
    
}
