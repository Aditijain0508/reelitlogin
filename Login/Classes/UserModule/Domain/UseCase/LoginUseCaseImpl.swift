//
//  LoginUseCaseImpl.swift
//  Login
//
//  Created by Sharvan Kumawat on 29/06/22.
//

import Foundation

class LoginUseCaseImpl: ILoginUseCase {
    private let repository: ILoginRepository
    
    init(repository: ILoginRepository) {
        self.repository = repository
    }
    
    func getLogin(email: String, password: String) -> UserResponse {
        return repository.makeServiceCallToLoginUser(email: email, password: password)
    }
    
    func getRegister(email: String, password: String) -> UserResponse {
        return repository.makeServiceCallToLoginUser(email: email, password: password)
    }
}
