//
//  ILoginUseCase.swift
//  Login
//
//  Created by Sharvan Kumawat on 25/07/22.
//

import Foundation
import PromiseKit
import FirebaseAuth

typealias UserResponse = Promise<Bool>

protocol ILoginUseCase {
    func getLogin(email: String, password: String) -> UserResponse
}
