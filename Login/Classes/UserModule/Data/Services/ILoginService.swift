//
//  ILoginService.swift
//  Login
//
//  Created by Sharvan Kumawat on 25/07/22.
//

import Foundation

protocol ILoginService {
    func makeNetworkRequest(email: String, password: String) -> UserResponse
}
