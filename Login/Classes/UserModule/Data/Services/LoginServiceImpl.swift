//
//  LoginServiceImpl.swift
//  Login
//
//  Created by Sharvan Kumawat on 25/07/22.
//

import Foundation
import Network
import FirebaseAuth
import PromiseKit

class LoginServiceImpl: ILoginService {
    
    func makeNetworkRequest(email: String, password: String) -> UserResponse {
        
        return Promise<Bool> { seal in
            Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
                if let error = error {
                    seal.reject(error)
                }
                else {
                    seal.fulfill(true)
                }
            }
        }
    }
}
