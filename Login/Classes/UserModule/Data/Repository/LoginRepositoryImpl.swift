//
//  LoginRepositoryImpl.swift
//  Login
//
//  Created by Sharvan Kumawat on 25/07/22.
//

import Foundation

class LoginRepositoryImpl: ILoginRepository {
    
    private let service: ILoginService
    
    init(service: ILoginService) {
        self.service = service
    }
    
    func makeServiceCallToLoginUser(email: String, password: String) -> UserResponse {
        return service.makeNetworkRequest(email: email, password: password)
    }
    
}
