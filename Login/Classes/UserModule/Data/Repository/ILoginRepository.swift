//
//  ILoginRepository.swift
//  User
//
//  Created by Sharvan Kumawat on 25/07/22.
//

import Foundation

protocol ILoginRepository {
    func makeServiceCallToLoginUser(email: String, password: String) -> UserResponse
}
