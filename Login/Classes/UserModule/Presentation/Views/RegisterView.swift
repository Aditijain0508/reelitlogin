//
//  RegisterView.swift
//  Login
//
//  Created by Sharvan Kumar Kumawat on 24/07/22.
//

import SwiftUI

struct RegisterView : View {
    
    @State var username: String = ""
    @State var password: String = ""
    @State var showAlert: Bool = false
    @Environment(\.presentationMode) var presentation
    var viewModel = RegisterViewModel()
    
    var body: some View {
        
        VStack {
            WelcomeText()
            UserImage()
            TextField("Enter Email", text: $username)
                .padding()
                .background(lightGreyColor)
                .cornerRadius(5.0)
                .padding(.bottom, 20)
            SecureField("Enter Password", text: $password)
                .padding()
                .background(lightGreyColor)
                .cornerRadius(5.0)
                .padding(.bottom, 20)
            Button(action: {
                viewModel.registerUser(email: username, password: password, completion: { result in
                    if result {
                        presentation.wrappedValue.dismiss()
                    } else {
                        showAlert.toggle()
                    }
                })
            }) {
                ButtonContent(text: "SUBMIT")
            }.padding()
        }
        .padding()
        .alert(isPresented: $showAlert) {
            Alert(title: Text(viewModel.errorMessage),
                  message: nil,
                  dismissButton: .default(Text("OK")))
        }
    }
}

#if DEBUG
struct RegisterView_Previews : PreviewProvider {
    static var previews: some View {
        RegisterView()
            .previewInterfaceOrientation(.portrait)
    }
}
#endif
