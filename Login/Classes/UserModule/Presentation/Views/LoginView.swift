//
//  LoginView.swift
//  Login
//
//  Created by Sharvan Kumar Kumawat on 26/07/22.
//

import SwiftUI

let lightGreyColor = Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0)

struct LoginView : View {
    
    @State var email: String = ""
    @State var password: String = ""
    @State var isNewUser: Bool = false
    
    var viewModel: ILoginViewModel?
    
    var body: some View {
        
        VStack {
            WelcomeText()
            UserImage()
            TextField("Enter Email", text: $email)
                .padding()
                .background(lightGreyColor)
                .cornerRadius(5.0)
                .padding(.bottom, 20)
            SecureField("Enter Password", text: $password)
                .padding()
                .background(lightGreyColor)
                .cornerRadius(5.0)
                .padding(.bottom, 20)
            Button(action: {
                viewModel?.login(email: email, password: password)
            }) {
                ButtonContent(text: "LOGIN")
            }.padding()
            
            Spacer()
            NavigationLink(
                destination:
                    RegisterView(),
                isActive: $isNewUser,
                label: {
                    HStack(spacing: 0) {
                        Text("Don't have an account?")
                        Button(action: {
                            isNewUser.toggle()
                        }) {
                            Text(" Sign Up")
                                .foregroundColor(.black)
                        }
                    }
                })
        }
        .padding()
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        LoginView()
            .previewInterfaceOrientation(.portrait)
    }
}
#endif

struct WelcomeText : View {
    var body: some View {
        return Text("Welcome Reelit!")
            .font(.largeTitle)
            .fontWeight(.semibold)
            .padding(.bottom, 20)
    }
}

struct UserImage : View {
    var body: some View {
        return Image.init("userImage", bundle: Bundle.current)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 150, height: 150)
            .clipped()
            .cornerRadius(150)
            .padding(.bottom, 75)
    }
}

struct ButtonContent : View {
    @State var text: String = ""
    var body: some View {
        return Text(text)
            .font(.headline)
            .foregroundColor(.white)
            .padding()
            .frame(width: 300, height: 60)
            .background(Color.green)
            .cornerRadius(15.0)
    }
}

extension Bundle {
    static var current: Bundle {
        class __ { }
        return Bundle(for: __.self)
    }
}
