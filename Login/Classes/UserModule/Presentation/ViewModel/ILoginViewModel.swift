//
//  ILoginViewModel.swift
//  Login
//
//  Created by Sharvan Kumawat on 25/07/22.
//

import Foundation

protocol ILoginViewModel {
    func login(email: String, password: String)
    var outputDelegate: LoginViewModelOutput? { get set }
}
