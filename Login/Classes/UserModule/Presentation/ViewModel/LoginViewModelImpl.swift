//
//  LoginViewModelImpl.swift
//  Login
//
//  Created by Sharvan on 25/07/22.
//

import Foundation

class LoginViewModelImpl: ILoginViewModel {
    
    var outputDelegate: LoginViewModelOutput?
    
    private let useCase: ILoginUseCase
    
    func login(email: String, password: String) {
        useCase.getLogin(email: email, password: password)
            .done(on: .main) { [weak self] model in
                debugPrint("Success ===> ", model)
                self?.outputDelegate?.success()
            }
            .catch(on: .main, policy: .allErrors) { [weak self] error in
                self?.outputDelegate?.gotError(error.localizedDescription)
            }
    }
    
    init(useCase: ILoginUseCase) {
        self.useCase = useCase
    }
    
}
