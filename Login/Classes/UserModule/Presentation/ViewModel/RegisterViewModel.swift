//
//  RegisterViewModel.swift
//  Login
//
//  Created by Sharvan Kumawat on 27/07/22.
//

import Foundation
import FirebaseAuth

class RegisterViewModel: ObservableObject {
    var errorMessage = ""
    init() {}
    
    func registerUser(email: String, password: String, completion: @escaping (Bool) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if error == nil {
                completion(true)
            } else {
                self.errorMessage = error?.localizedDescription ?? ""
                completion(false)
            }
        }
    }
}
