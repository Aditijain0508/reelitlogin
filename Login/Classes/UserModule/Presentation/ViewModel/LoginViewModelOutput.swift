//
//  LoginViewModelOutput.swift
//  Login
//
//  Created by Sharvan Kumawat on 25/07/22.
//

import Foundation

protocol LoginViewModelOutput {
    func success()
    func gotError(_ error: String)
}
