//
//  LoginViewController.swift
//  Login
//
//  Created by Aditi Jain 3 on 28/05/22.
//

import UIKit
import Network
import Core
import SwiftUI

class LoginViewController: UIViewController {
    let networkManager = NetworkManager()
    var viewModel: ILoginViewModel?

    override func loadView() {
        let loginViewController = UIHostingController(rootView: LoginView(viewModel: viewModel))
        view = UIView()
        self.addChild(loginViewController)
        view.addSubview(loginViewController.view)
        loginViewController.view.frame = view.bounds
        loginViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        loginViewController.didMove(toParent: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
}


extension LoginViewController: Alertable, LoginViewModelOutput {
    
    func success() {
        //TODO -- Go to browse module
    }
    
    func gotError(_ error: String) {
        showAlert(message: error, on: self)
    }
    
}
