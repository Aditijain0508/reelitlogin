fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## iOS

### ios lint

```sh
[bundle exec] fastlane ios lint
```



### ios test

```sh
[bundle exec] fastlane ios test
```



### ios beta_build

```sh
[bundle exec] fastlane ios beta_build
```



### ios beta_build_appcenter

```sh
[bundle exec] fastlane ios beta_build_appcenter
```



### ios upload_on_debug_appcenter

```sh
[bundle exec] fastlane ios upload_on_debug_appcenter
```



----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
