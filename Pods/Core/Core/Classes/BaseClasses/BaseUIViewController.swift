//
//  BaseUIViewController.swift
//  Core
//
//  Created by Aditi Jain 3 on 28/05/22.
//

import Foundation
import UIKit

open class BaseViewController: UIViewController {
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        print("In Base View Controller")
    }
    
}
